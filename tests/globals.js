var selectors = {
  textSelector: "input#input-text",
  dropdownSelector: "select#dropdown-content",
  buttonSelector: "button#translate-button",
  outputSelector: "h1#output-text"
};

module.exports.selectors = selectors;
module.exports.textSelector = selectors.textSelector;
module.exports.dropdownSelector = selectors.dropdownSelector;
module.exports.buttonSelector = selectors.buttonSelector;
module.exports.outputSelector = selectors.outputSelector;
