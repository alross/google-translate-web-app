"use strict";

module.exports = {
  "Page Loaded Tests": function(client) {
    const dirName = __dirname;
    const fileLocation = "../../../ui/index.html";
    client.url("file://" + dirName + fileLocation);
    client.waitForElementVisible("body", 2000);
  },
  "Test Spanish": function(client) {
    client.setValue(client.globals.textSelector, "Hello");
    client.assert
      .visible(client.globals.dropdownSelector)
      .click(client.globals.dropdownSelector);
    client.useXpath();
    client.click(".//text()[contains(.,'spanish')]/..");
    client.useCss();
    client.click(client.globals.buttonSelector);
    client.assert.containsText(client.globals.outputSelector, "Hola");
  },
  "Test German": function(client) {
    client.assert
      .visible(client.globals.dropdownSelector)
      .click(client.globals.dropdownSelector);
    client.useXpath();
    client.click(".//text()[contains(.,'german')]/..");
    client.useCss();
    client.click(client.globals.buttonSelector);
    client.assert.containsText(client.globals.outputSelector, "Hallo");
  },
  "Test French": function(client) {
    client.assert
      .visible(client.globals.dropdownSelector)
      .click(client.globals.dropdownSelector);
    client.useXpath();
    client.click(".//text()[contains(.,'french')]/..");
    client.useCss();
    client.click(client.globals.buttonSelector);
    client.assert.containsText(client.globals.outputSelector, "Bonjour");
  }
};
