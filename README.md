# README #

  
  

### What is this repository for? 


* A simple web app that demonstrates the power of the Google Translate API.

* UI built by Alex Ross 

* Web Service written by Ben Mumma

* Tests written using nightwatch.js by Jonah Vallance

##  Getting Started:

* Install Node.js [https://nodejs.org/en/download/](https://nodejs.org/en/download/)

###  Web Service:

 - In Terminal/Command Prompt, navigate to this repo and the service folder
 - Type: **npm install** 
 - To start web app, type: **node app**

###  UI:
 * After web service has been started, double click index.html in the UI folder
###  Testing:
* Navigate in Terminal/Command to this repo and the test folder
* Type: **npm install**
 - To test web app, start the web app and use: **npm run test-e2e** 
