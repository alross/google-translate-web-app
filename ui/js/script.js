// URLs for API calls
const getUrl = new URL("http://localhost:5000/languages");
const translateUrl = new URL("http://localhost:5000/translate");

// parameters for translate request
let translateParams;

// currently selected language
let selectedLanguage;

// dropdown selection menu DOM element
let dropdown;

// default option in selection dropdown menu (Translate...)
let defaultOption;

/**
 * Toggle selected language
 * @param {Element} selection Language currently selected in selection list
 */
function selectLanguage(selection) {
  selectedLanguage = selection;
}

/**
 * Use fetch framework to perform API calls to translate text
 */
function translateText() {
  // parameters for translate request
  translateParams = {
    q: document.getElementById("input-text").value,
    target: selectedLanguage.value
  };

  // execute POST language translation
  fetch(translateUrl, {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    method: "POST",
    body: JSON.stringify(translateParams)
  })
    .then(function(response) {
      // check for 200 OK status code
      if (response.status !== 200) {
        console.warn("Some issue occurred. Status Code: " + response.status);
        return;
      }

      // Output the text in the response
      response.json().then(function(rawData) {
        const data = rawData.data.translations[0].translatedText;

        document.getElementById("output-text").innerText = data;
      });
    })
    .catch(function(err) {
      console.error("Fetch Error - ", err);
    });
}

window.onload = function() {
  // retrieve selection dropdown
  dropdown = document.getElementById("dropdown-content");
  dropdown.length = 0;

  // initialize selection dropdown
  defaultOption = document.createElement("option");
  defaultOption.text = "Translate...";

  dropdown.add(defaultOption);
  dropdown.selectedIndex = 0;

  // execute GET request for list of languages
  fetch(getUrl)
    .then(function(response) {
      if (response.status !== 200) {
        console.warn(
          "Looks like there was a problem. Status Code: " + response.status
        );
        return;
      }

      // Examine text in the response
      response.json().then(function(rawData) {
        let data = rawData.language;
        let option;

        // iteratively add language options according to JSON response
        for (let i = 0; i < data.length; i++) {
          option = document.createElement("option");
          option.text = data[i].name;
          option.value = data[i].code;
          dropdown.add(option);
          console.log(option);
        }
      });
    })
    .catch(function(err) {
      console.error("Fetch Error - ", err);
    });
};
