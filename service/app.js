const express = require("express"); // imports (requires) the use of NPM module express.
const util = require("util"); // imports (requires) the use of NPM module util.
const app = express(); // instantiates express. setting up express to run for the first time.
const bodyParser = require("body-parser"); // imports (requires) the use of the body parser.
const request = require("request"); // / imports (requires) the use of the NPM module request.
const cors = require("cors"); // imports (requires) the use of the NPM module cors.

const PORT = process.env.PORT || 5000; // accepts whatever the server is passing, or 5000.

// middleware (from outside sources).
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

// sets a language route that takes in the request and the response. It sets the name and passes the language code both to the UI.
app.get("/languages", (req, res) => {
  // sets the response.
  const jsonResponse = {
    language: [
      { name: "spanish", code: "es" }, // properties of language[]. name, followed by Google API language code.
      { name: "french", code: "fr" },
      { name: "german", code: "de" },
      { name: "galician", code: "gl" },
      { name: "greek", code: "el" },
      { name: "italian", code: "it" },
      { name: "chinese simplified", code: "zh-CN" },
      { name: "croatian", code: "hr" },
      { name: "czech", code: "cs" },
      { name: "danish", code: "da" },
      { name: "dutch", code: "nl" },
      { name: "georgian", code: "ka" },
      { name: "irish", code: "ga" },
      { name: "japanese", code: "ja" }
    ]
  };
  // stores jsonResponse.
  res.json(jsonResponse);
});
/*
posts to the translate route.
sets the variable q to the request body.
sets the target body to the request.
*/
app.post("/translate", (req, res) => {
  const q = req.body.q;
  const target = req.body.target;
  // the URI is formattted with the built in fucntion of the util module. It replaces the %s with "q" and "target" respectively.
  let uri = util.format(
    "https://translation.googleapis.com/language/translate/v2?key=AIzaSyAPWe3Hla3PEMWbiOGHpMihBKnSbQTXvvY&q=%s&target=%s",
    q,
    target
  );
  // sets the method to POST (post to the API) and the url = uri.
  var options = {
    method: "POST",
    url: uri
  };

  // requests options and a function that responds in the case of an error. The error is logged to the console and displayed to the user. Response is then parsed.
  request(options, function(error, response, body) {
    if (error) throw new Error(error);

    console.log(body);

    res.json(JSON.parse(body));
  });
});
// tells the user that the translation service is running in the terminal.
app.listen(PORT, () => console.log("Translation service running"));
